# use alt+NUM like in windows

for i in {1..255}; do
  printf -v char %03o "$i"
  printf -v char "\\$char"
  case ${#i} in
    1) key="\\e$i" ;;
    2) key="\\e${i:0:1}\\e${i:1:1}" ;;
    3) key="\\e${i:0:1}\\e${i:1:1}\\e${i:2:1}" ;;
  esac
  # or maybe just set both...
  if [[ -o emacs ]]; then
    bind "\"$key\":\"$char\""
  else
    bind -m vi-insert "\"$key\":\"$char\""
  fi
done